
import 'package:dartcas/models/operators/OperatorBuilder.dart';
import 'package:dartcas/models/values/NumberValue.dart';
import 'package:dartcas/models/operators/ValueOperator.dart';
import 'package:dartcas/models/values/Fraction.dart';
import 'package:dartcas/models/values/Value.dart';
import 'package:dartcas/parser/StringParser/StringParser.dart';
import 'package:dartcas/models/operators/Multiplication.dart';
import 'package:dartcas/models/operators/Sum.dart';
import 'package:dartcas/models/operators/Division.dart';
import 'package:dartcas/models/operators/Operator.dart';
import 'package:test/test.dart';

void main() {
  test('Tests sum and valueAction', () {
    NumberValue n = NumberValue(3);

    ValueOperator act =  ValueOperator(value: n);
    Sum inner = Sum(self: act, other: act);

    Sum s = Sum(self: act, other: inner);

    expect(s.getValue().toInt(), equals(9));
  });

  test('Test sum, valueAction, Division and Multiplication with numberValue', () {
    NumberValue n = NumberValue( 3);
    NumberValue n1 = NumberValue( 5);

    ValueOperator act =  ValueOperator(value: n);

    Multiplication mult = Multiplication(self: ValueOperator(value: n1), other: act);
    Sum inner = Sum(self: act, other: act);
    Sum s = Sum(self: mult, other: inner);
    Division div = Division(self: s, other: mult,);

    expect(mult.getValue().toInt(), equals(15));
    expect(inner.getValue().toInt(), equals(6));
    expect(s.getValue().toInt(), equals(21));
    expect(div.getValue().toString(), equals("7/5"));

  }); 

  test('Test fraction clean', () {

    Fraction f = Fraction(numerator: NumberValue(5), denominator: NumberValue(2));
    Fraction f2 = Fraction(numerator: NumberValue(9), denominator: NumberValue(3));
    Fraction f3 = Fraction(numerator: NumberValue(45), denominator: NumberValue(10));
    Fraction f4 = Fraction(numerator: NumberValue(-45), denominator: NumberValue(10));

    expect(f.clean().toString(), equals("5/2"));

    expect(f2.clean().toString(), equals("3"));

    expect(f3.clean().toString(), equals("9/2"));
    expect(f4.clean().toString(), equals("-9/2"));

  }); 

  test('Test fraction calculus', () {

    Fraction f = Fraction(numerator: NumberValue(5), denominator: NumberValue(2));
    Fraction f2 = Fraction(numerator: NumberValue(97), denominator: NumberValue(23));
    Fraction f3 = Fraction(numerator: NumberValue(45), denominator: NumberValue(10));

    NumberValue n = NumberValue(5);
    expect((f+f3).toString(), equals("7"));
    expect((f+n).toString(), equals("15/2"));
    expect((f*f3).toString(), equals("45/4"));
    expect((f*n).toString(), equals("25/2"));

    expect((f-f3).toString(), equals("-2"));
    expect((f+n-f3).toString(), equals("3"));
    expect((f-f2).toString(), equals("-79/46"));
    expect((f/f2).toString(), equals("115/194"));

  });

  test('Test sum, valueAction and Multiplication with fractions', () {

    Fraction f = Fraction(numerator: NumberValue(5), denominator: NumberValue(2));
    Fraction f1 = Fraction(numerator: NumberValue(45), denominator: NumberValue(10));

    ValueOperator act = ValueOperator(value: f);
    ValueOperator act1 = ValueOperator(value: f1);

    Multiplication mult = Multiplication(self: act1, other: act);
    Sum inner = Sum(self: act, other: act);
    Sum s = Sum(self: mult, other: inner);

    expect(mult.getValue().toString(), equals("45/4"));
    expect(inner.getValue().toString(), equals("5"));

    expect(s.getValue().toString(), equals("65/4"));
  });

  test("String parser", (){
    String toBeParsed = "4+5+6";
    String toBeParsed1 = "4+5/2+6";
    String toBeParsed2 = "4/4/4";
    String toBeParsed3 = "5^4/2";

    StringParser parser = StringParser();
    Operator a = parser.getParsed(toBeParsed);
    expect(a.getValue().toInt(), equals(15));

    Operator a1 = parser.getParsed(toBeParsed1);
    expect(a1.getValue().toString(), equals("25/2"));

    Operator a2 = parser.getParsed(toBeParsed2);
    expect(a2.getValue().toString(), equals("1/4"));

    Operator a3 = parser.getParsed(toBeParsed3);
    expect(a3.getValue().toString(), equals("625/2"));
});

test("Test functions", (){
    String toBeParsed = "sin(0)+1";
    String toBeParsed1 = "cos(0)";
  

    StringParser parser = StringParser();
    Operator a = parser.getParsed(toBeParsed);
    expect(a.getValue().toInt(), equals(1));

    Operator a1 = parser.getParsed(toBeParsed1);
    expect(a1.getValue().toString(), equals("1.0"));
  });


  test("Test more advanced functions", (){

    String toBeParsed = "sin(0)+cos(cos(0+1)+0)";
    String toBeParsed1 = "cos(0)";
    String toBeParsed2 = "5<<5";
    String toBeParsed3 = "1+max(1,2,4,4,5,6,7,min(4+7,8))";

    StringParser parser = StringParser(operatorBuilders: [OperatorBuilder("<<", 6, (Operator self, Operator other){
      return BitShift(self, other, "<<", 5);
    })]);

    Operator a = parser.getParsed(toBeParsed);
    expect(a.getValue().toInt(), equals(1));

    Operator a1 = parser.getParsed(toBeParsed1);
    expect(a1.getValue().toString(), equals("1.0"));
   
    Operator a2 = parser.getParsed(toBeParsed2);
    expect(a2.getValue().toString(), equals("160"));

    Operator a3 = parser.getParsed(toBeParsed3);
    expect(a3.getValue().toString(), equals("9"));
  });


  test("Test symbols", (){

    String toBeParsed = "sin(pi)";
 

    StringParser parser = StringParser();

    Operator a = parser.getParsed(toBeParsed);
    expect(a.getValue().toInt(), equals(0));
  });
}

class BitShift extends Operator{
  BitShift(Operator self, Operator other, String symbol, int importance) : super(self, other, symbol, importance);

  @override
  Value getValue() {
    return NumberValue(self.getValue().toInt()<<other.getValue().toInt());
  }

}
