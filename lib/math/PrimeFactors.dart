import 'dart:math';


bool isPrime(int number) {
  bool isPrime = true;
  for (int j = 2; j <= sqrt(number.abs()); j++) {
    if (number.abs() % j == 0) {
      isPrime = false;
    }
  }
  return isPrime;
}

List<int> getPrimeFactors(int number) {
  int absolute = number.abs();
  
  List<int> primes = [];
  int i = 2;

  while (i <= sqrt(absolute)) {

    if (absolute % i == 0) {
      bool prime = isPrime(i);

      absolute = (absolute / i).round();
      if (prime) {
        primes.add(i);
      }
    } else {
      i++;
    }
  }
  if(number.isNegative){
    primes.addAll([absolute,-1]);
  }else{
    primes.add(absolute);
  }

  return primes;
}
