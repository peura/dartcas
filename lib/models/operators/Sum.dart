import 'package:dartcas/models/values/Value.dart';

import 'Operator.dart';

class Sum extends Operator {
  Sum({self, other}) : super(self, other, "+", 2);

  @override
  Value getValue() {
    return self.getValue() + other.getValue();
  }
}

class Detraction extends Operator {
  Detraction({self, other}) : super(self, other, "+", 2);

  @override
  Value getValue() {
    return self.getValue() - other.getValue();
  }
}
