import 'package:dartcas/models/values/Value.dart';

abstract class Operator {
  final Operator self;
  final Operator other;
  final String symbol;
  final int importance;

  Operator(this.self, this.other, this.symbol, this.importance);
  Value getValue();
}