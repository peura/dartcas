import 'Operator.dart';

typedef Operator OperatorBuilderFunction(Operator self, Operator other);

class OperatorBuilder{
  final String symbol;
  final int priority;
  final OperatorBuilderFunction buildOperator;

  OperatorBuilder(this.symbol, this.priority, this.buildOperator);
}