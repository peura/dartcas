import 'package:dartcas/models/values/Value.dart';

import 'Operator.dart';

class ValueOperator extends Operator {
  final Value value;

  ValueOperator({
    this.value,
  }) : super(null, null,"", 1);

  @override
  Value getValue() {
    return value;
  }
}
