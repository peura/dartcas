import 'package:dartcas/models/values/Value.dart';

import 'Operator.dart';

class Multiplication extends Operator {
  Multiplication({self, other}) : super(self, other, "*", 2);

  @override
  Value getValue() {
    return self.getValue() * other.getValue();
  }
}
