import 'package:dartcas/models/values/Value.dart';

import 'Operator.dart';

class Division extends Operator {
  Division({self, other}) : super(self, other, "/", 2);

  @override
  Value getValue() {
    return self.getValue() / other.getValue();
  }
}
