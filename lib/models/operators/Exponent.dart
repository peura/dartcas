import 'package:dartcas/models/values/Value.dart';
import 'Operator.dart';

class Exponent extends Operator {
  Exponent({Operator self, Operator other}) : super(self, other, "^", 3);

  @override
  Value getValue() {
    return self.getValue() ^ other.getValue();
  }
}