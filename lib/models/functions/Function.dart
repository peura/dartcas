import 'package:dartcas/models/values/Value.dart';

typedef Value functionRunner(List<Value> params);

class FunctionOperator {
  final String name;
  final functionRunner;

 const FunctionOperator({this.name, this.functionRunner});
}
