import 'dart:math';
import 'package:dartcas/models/functions/Function.dart';
import 'package:dartcas/models/values/Decimal.dart';
import 'package:dartcas/models/values/NumberValue.dart';
import 'package:dartcas/models/values/Value.dart';

final FunctionOperator brackets = FunctionOperator(
    functionRunner: (List<Value> value) {
      if (value.length > 1) {
        throw FunctionException(
            "Invalid number of params: ${value.length} at function ()");
      }
      return value[0];
    },
    name: "");

final FunctionOperator cosF = FunctionOperator(
    functionRunner: (List<Value> value) {
      if (value.length > 1) {
        throw FunctionException(
            "Invalid number of params: ${value.length} at function cos");
      }
      return Decimal(cos(value[0].round())).roundToIntIfPossible();
    },
    name: "cos");

final FunctionOperator sinF = FunctionOperator(
    functionRunner: (List<Value> value) {
      if (value.length > 1) {
        throw FunctionException(
            "Invalid number of params: ${value.length} at function sin");
      }
      return Decimal(sin(value[0].round())).roundToIntIfPossible();
    },
    name: "sin");

final FunctionOperator sqrtF = FunctionOperator(
    functionRunner: (List<Value> value) {
      if (value.length > 1) {
        throw FunctionException(
            "Invalid number of params: ${value.length} at function sin");
      }
      return Decimal(sqrt(value[0].toInt())).roundToIntIfPossible();
    },
    name: "sqrt");

final FunctionOperator max = FunctionOperator(
    functionRunner: (List<Value> values) {
      if (values.length < 1) {
        return NumberValue( 0);
      }

      values.sort((Value val1, Value val2) {
        double val1r = val1.round();
        double val2r = val2.round();
        if (val1r > val2r) {
          return 1;
        } else if (val1r < val2r) {
          return -1;
        }
        return 0;
      });

      return values.last;
    },
    name: "max");

    final FunctionOperator min = FunctionOperator(
    functionRunner: (List<Value> values) {
      if (values.length < 1) {
        return NumberValue(0);
      }

      values.sort((Value val1, Value val2) {
        double val1r = val1.round();
        double val2r = val2.round();
        if (val1r > val2r) {
          return 1;
        } else if (val1r < val2r) {
          return -1;
        }
        return 0;
      });

      return values.first;
    },
    name: "min");

class FunctionException implements Exception {
  FunctionException(this.cause);
  String cause;
}
