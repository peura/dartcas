abstract class Value {

  String toString();
  bool get isNumeric;

  int toInt();

  List<Value> factors();

  Value operator + (Value other);
  Value operator / (Value other);
  Value operator - (Value other);
  Value operator * (Value other);
  Value operator ^ (Value other);
  bool operator == (Object other);

  double round();

}