import 'dart:math';

import 'NumberValue.dart';
import 'Value.dart';

class Decimal extends Value {
  final double value;

  Decimal(this.value);
  static const minDouble = 1.2246477991473532e-16;

  @override
  bool get isNumeric => true;


  @override
  bool operator == (Object other){
    if(other is Decimal){
      return this.value==other.value;
    }
    else if(other is double|| other is int){
      return this.value==other;
    }
    return false;
  }

  Value roundToIntIfPossible() {
    if (value.roundToDouble() == this) {
      return NumberValue(this.toInt());
    }

    if (this.value < minDouble) {
      return NumberValue( 0);
    }

    return this;
  }

  @override
  String toString() => value.toString();

  @override
  Value operator *(Value other) {
    return Decimal(other.round() * value);
  }

  @override
  Value operator +(Value other) {
    return Decimal(other.round() + value);
  }

  @override
  Value operator -(Value other) {
    return Decimal(other.round() * value);
  }

  @override
  Value operator /(Value other) {
    return Decimal(other.round() / value);
  }

  @override
  Value operator ^(Value other) {
    return Decimal(pow(other.round(), value));
  }

  @override
  int toInt() => value.round();

  @override
  double round() => value;

  @override
  List<Value> factors() {
    return [this];
  }
}
