import 'package:dartcas/models/values/Decimal.dart';
import 'package:dartcas/models/values/NumberValue.dart';

import 'Value.dart';
import 'nonNumeric/Exponent.dart';
import 'nonNumeric/Polynom.dart';

class Fraction extends Value {
  final Value numerator, denominator;

  Fraction({this.numerator, this.denominator});

  @override
  bool get isNumeric => true;

  @override
  String toString() {
    Fraction clean = this.clean();
    return "${clean.numerator}/${clean.denominator}";
  }

  @override
  int toInt() {
    return (numerator.toInt() / denominator.toInt()).round();
  }

  @override
  double round() {
    return this.numerator.round() / this.denominator.round();
  }

  @override
  Value operator *(Value other) {
    if (other is Fraction) {
      Value newNumerator = this.numerator * other.numerator;
      Value newDenominator = this.denominator * other.denominator;
      return Fraction(numerator: newNumerator, denominator: newDenominator)
          .clean();
    }
    return Fraction(
            numerator: this.numerator * other, denominator: this.denominator)
        .clean();
  }

  @override
  Value operator +(Value other) {
    if (other is NumberValue || other is Decimal) {
      return Fraction(
              numerator: this.numerator + other * this.denominator,
              denominator: this.denominator)
          .clean();
    } else if (other is Fraction) {
      Value newNumerator = this.numerator * other.denominator +
          other.numerator * this.denominator;
      Value newDenominator = this.denominator * other.denominator;
      return Fraction(numerator: newNumerator, denominator: newDenominator)
          .clean();
    }
    return Polynom();
  }

  @override
  Value operator -(Value other) {
    if (other is NumberValue || other is Decimal) {
      return Fraction(
              numerator: this.numerator - other * this.denominator,
              denominator: this.denominator)
          .clean();
    } else if (other is Fraction) {
      Value newNumerator = this.numerator * other.denominator -
          other.numerator * this.denominator;
      Value newDenominator = this.denominator * other.denominator;
      return Fraction(numerator: newNumerator, denominator: newDenominator)
          .clean();
    }
    return Polynom();
  }

  @override
  bool operator ==(Object other) {
    if (other is Fraction) {
      return this.numerator == other.numerator &&
          this.denominator == other.denominator;
    } else if (other is double) {
      return this.round() == other;
    }
    return false;
  }

  @override
  Value operator /(Value other) {
    if (other is Fraction) {
      Value newNumerator = this.numerator * other.denominator;
      Value newDenominator = this.denominator * other.numerator;
      return Fraction(numerator: newNumerator, denominator: newDenominator)
          .clean();
    }
    return Fraction(
            numerator: this.numerator, denominator: this.denominator * other)
        .clean();
  }

  Value clean() {

    List<Value> numeratorPrimes = numerator.factors();
    List<Value> denominatorPrimes = denominator.factors();

    List<Value> uniqueNumeratorPrimes =
        _deleteCommonFactors(numeratorPrimes, denominatorPrimes);
    List<Value> uniqueDenominatorPrimes =
        _deleteCommonFactors(denominatorPrimes, numeratorPrimes);

    if (uniqueNumeratorPrimes.isEmpty) {
      uniqueNumeratorPrimes = [NumberValue.one()];
    }

    Value newNumerator = uniqueNumeratorPrimes
        .reduce((Value value, Value element) => value * element);

    if (uniqueDenominatorPrimes.isEmpty) {
      return newNumerator;
    }

    Value newDenumerator = uniqueDenominatorPrimes
        .reduce((Value value, Value element) => value * element);

    return Fraction(
        numerator: newNumerator, denominator: newDenumerator); 
  }

  List<Value> _deleteCommonFactors(List<Value> list, List<Value> values) {
    List<Value> vals = []..addAll(list);
    values.forEach((Value element) {
      vals.remove(element);
    });
    return vals;
  }

  @override
  Value operator ^(Value other) {

    if (other is NumberValue) {
      Value newNumerator = this.numerator^other;
      Value newDenumerator = this.denominator^other;
      return Fraction(numerator: newNumerator, denominator: newDenumerator);
    }
    return ExponentValue(this, other, NumberValue.one());
  }

  @override
  List<Value> factors() {
    List<Value> denominatorFactors = denominator.factors();
    denominatorFactors.map((e) => Fraction(numerator: NumberValue.one(), denominator: e));
    return [...denominatorFactors, ...numerator.factors()];
  }
}
