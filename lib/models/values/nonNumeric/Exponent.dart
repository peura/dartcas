import 'dart:math';

import 'package:dartcas/models/values/NumberValue.dart';
import 'package:dartcas/parser/StringParser/StringParser.dart';

import '../Value.dart';
import 'Polynom.dart';



class ExponentValue extends Value {

  final Value base;
  final Value exponent;
  final Value multiplier;

  @override
  bool get isNumeric => false; 

  ExponentValue(this.base, this.exponent, this.multiplier){
    if(!_containsAllowedTypes()){
      throw ExponentParameterException("Prohibited type combination for base: ${base.runtimeType} and exponent: ${exponent.runtimeType}");
    }
  }

  @override
  bool operator == (Object other){
    if(other is ExponentValue){
      return this.base==other.base&&this.exponent==other.exponent&&this.multiplier==other.multiplier;
    }
    return false;
  }

  @override
  String toString() {
    if(multiplier==NumberValue.one()){
      return "("+base.toString()+")^("+exponent.toString()+")";
    }
    return multiplier.toString()+"*("+base.toString()+")^("+exponent.toString()+")";
  }

  bool _containsAllowedTypes(){
    if(base is Polynom){
      return !(exponent is NumberValue) && !(multiplier is Polynom);
    }
    return true;
  }

  Value clean(){
    if(this.multiplier==NumberValue.zero()){
      return NumberValue.zero();
    }
    if(this.exponent==NumberValue.one()){
      return this.multiplier*this.base;
    }
    if(this.exponent==NumberValue.zero()){
      return this.multiplier;
    }
    if(this.exponent is NumberValue && this.base is NumberValue){
      return multiplier*(this.base^this.exponent);
    }
    return this;
  }

  @override
  Value operator *(Value other) {
    if(other is ExponentValue){
      if(other==this){
        return ExponentValue(base, other.exponent+this.exponent, this.multiplier*other.multiplier).clean();
      }
      throw UnimplementedError();
    }
    if(other is Polynom){
      return other*this;
    }
    if(other==this.base){
      return ExponentValue(base, this.exponent+NumberValue.one(), this.multiplier).clean();
    }

    return ExponentValue(base, exponent, multiplier*other).clean();
  }

  @override
  Value operator +(Value other) {
    if(other is ExponentValue){
      if(this==other){
        return ExponentValue(base, exponent, this.multiplier+other.multiplier).clean();
      }
      throw UnimplementedError();
    }
    throw UnimplementedError();
  }

  @override
  Value operator -(Value other) {
    if(other is ExponentValue){
      if((this.multiplier-other.multiplier)==NumberValue.zero()){
        return NumberValue.zero();
      }
      if(other==this){
        return ExponentValue(base, exponent, this.multiplier-other.multiplier).clean();
      }
    }
    throw UnimplementedError();
  }

  @override
  Value operator /(Value other) {
   if(other is ExponentValue){
      if(other==this){
        return ExponentValue(base, exponent-other.exponent, this.multiplier/other.multiplier).clean();
      }
    }
    if(other is Polynom){
      return ExponentValue(other, NumberValue(-1), this).clean();
    }
    return ExponentValue(base, exponent, this.multiplier/other).clean();
  }

  @override
  Value operator ^(Value other) {
    return ExponentValue(base, exponent*other, multiplier).clean();
  }

  @override
  double round() {
    return pow(base.round(), exponent.round());
  }

  @override
  int toInt() {
    return pow(base.round(), exponent.round()).round();
  }

  @override
  List<Value> factors() {
    return base.factors();
  }

}

