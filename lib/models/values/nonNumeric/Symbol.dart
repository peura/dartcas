import 'dart:math' as Math;


import 'package:dartcas/models/values/NumberValue.dart';
import 'package:dartcas/models/values/nonNumeric/Polynom.dart';

import '../Decimal.dart';
import '../Value.dart';
import 'Exponent.dart';

typedef Symbol builder();

abstract class Symbol extends Value {

  Value multiplier = NumberValue.one();

  Symbol(this.symbol);

  @override
  bool get isNumeric => false;

  @override
  Value operator *(Value other) {
    if(other is Symbol&&other==this){
      this.multiplier*=other.multiplier;
      return ExponentValue(this.multiplier, this, NumberValue(2));
    }
    this.multiplier*=other;
    return this;
  }
  
  @override
  Value operator +(Value other) {
    if(other is Symbol&&other==this){
      this.multiplier+=other.multiplier;
      return this;
    }
    return Polynom();
  }
  
  @override
  Value operator -(Value other) {
   if(other is Symbol&&other==this){
      this.multiplier-=other.multiplier;
      return this;
    }
    return Polynom();
  }
  
  @override
  Value operator /(Value other) {
    if(other is Symbol&&other==this){
      return this.multiplier/other.multiplier;
    }
  }

  @override
  Value operator ^(Value other) {
    return Decimal(Math.pow(Math.pi, other.round()));
  }


  @override
  bool operator == (Object other){
    if(other is Symbol){
      return this.symbol==other.symbol;
    }
    return false;
  }

  @override
  List<Value> factors() {
    //Todo: remove multiplier
    return [this, multiplier];
  }

  final String symbol;
}

class SymbolBuilder{
  final String symbol;
  final builder build;

  SymbolBuilder(this.symbol, this.build);
}

class pi extends Symbol{

    pi(String symbol) : super(symbol);

    static SymbolBuilder builder(){
      return SymbolBuilder("pi", (){
        return pi("pi");
      });
    }

    @override
    Value operator *(Value other) {
      return Decimal(other.round()*Math.pi);
    }
  
    @override
    Value operator +(Value other) {
      return Decimal(other.round()+Math.pi);
    }
  
    @override
    Value operator -(Value other) {
      return Decimal(Math.pi-other.round());
    }
  
    @override
    Value operator /(Value other) {
      return Decimal(Math.pi/other.round());
    }
  
    @override
    Value operator ^(Value other) {
      return Decimal(Math.pow(Math.pi, other.round()));
    }

  @override
  double round() {
    return Math.pi;
  }

  @override
  int toInt() {
    return 3;
  }

}