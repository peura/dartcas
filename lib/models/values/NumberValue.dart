import 'dart:math';

import 'package:dartcas/math/PrimeFactors.dart';
import 'package:dartcas/models/values/Decimal.dart';
import 'package:dartcas/models/values/Fraction.dart';
import 'package:dartcas/models/values/Value.dart';

import 'nonNumeric/Exponent.dart';

class NumberValue extends Value {
  
  final int value;

  NumberValue(this.value);

  @override
  int toInt() => value;

  @override
  String toString() => value.toString();

  @override
  bool operator == (Object other){
    if(other is NumberValue){
      return this.value==other.value;
    }
    else if(other is int || other is double){
      return this.value==other;
    }
    return false;
  }

  @override
  Value operator +(Value other) {
    if (other is Fraction) {
      return other + this;
    } else if (other is Decimal) {
      return Decimal(this.round() + other.value);
    } else if (other is NumberValue) {
      return NumberValue(this.value + other.value);
    } else if (other is Symbol) {
      return Decimal(this.round() + other.round());
    }
    return other + this;
  }

  @override
  Value operator *(Value other) {
    if (other is Fraction) {
      return other * this;
    } else if (other is NumberValue) {
      return NumberValue(this.value * other.value);
    } else if (other is Symbol) {
      return Decimal(this.round() * other.round());
    }
    return other * this;
  }

  @override
  Value operator / (Value other) {
    return Fraction(numerator: this, denominator: other).clean();
  }

  @override
  Value operator -(Value other) {
    if (other is Fraction) {
      return other - this;
    } else if (other is NumberValue) {
      return NumberValue(this.value - other.value);
    } else if (other is Symbol) {
      return Decimal(this.round() - other.round());
    }
    return NumberValue( -1) * other + this;
  }

  static NumberValue zero(){
    return NumberValue(0);
  }

  static NumberValue one(){
    return NumberValue(1);
  }

  @override
  Value operator ^(Value other) {
    if (other is Fraction) {
      return ExponentValue(this, other, NumberValue.one());
    } else if (other is NumberValue) {
      return NumberValue(pow(this.value, other.value));
    }
    return Decimal(pow(this.round(), other.round()));
  }

  @override
  double round() => value.toDouble();

  @override
  bool get isNumeric => true;

  @override
  List<Value> factors() {
    return getPrimeFactors(this.value).map((e) => NumberValue(e)).toList();
  }
}
