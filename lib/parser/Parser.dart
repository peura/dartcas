import 'package:dartcas/models/operators/Operator.dart';

abstract class Parser{
  const Parser();
  Operator getParsed(String calculation);
}