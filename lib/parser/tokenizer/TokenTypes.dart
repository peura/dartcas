
import 'package:dartcas/models/values/Value.dart';

abstract class Token {}

class OperatorToken extends Token {

  final String token;
  final OperatorSymbol action;
  final int priority;

  @override
  String toString() {
    return "Operator: ${action.toString()}";
  }

  OperatorToken(this.action, this.priority, [this.token]);
}

class ValueToken extends Token {
  final Value value;

  @override
  String toString() {
    return "Value: ${value.toString()}";
  }

  ValueToken(this.value);
}

class FunctionToken extends Token {
  final String name;
  final List<List<Token>> params;

  @override
  String toString() {
    return "Function ${name.toString()}: ${params.toString()}";
  }

  FunctionToken(this.name, this.params);
}

enum OperatorSymbol {
  add,
  detract,
  divide,
  multiply,
  exponent,
  custom,
  comma,
  none,
}
