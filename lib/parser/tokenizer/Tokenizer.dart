import 'package:dartcas/models/operators/OperatorBuilder.dart';
import 'package:dartcas/models/values/NumberValue.dart';
import 'package:dartcas/models/values/nonNumeric/Symbol.dart';
import 'package:dartcas/parser/StringParser/StringParser.dart';

import 'FunctionTokenizer.dart';
import 'TokenTypes.dart';

/*
The default tokenizer for functions. This is highly inefficient and is to be replaced in future release.
*/

typedef bool CompareFunction(Object obj1);

extension Flatten on List<List> {
  List flatten (){
    return this.expand((i) => i).toList();
  }
}

List<Token> tokenize(String token, List<OperatorBuilder> operators, List<SymbolBuilder> symbols) {
  RegExp numbersAndStrings = RegExp(r"([0-9]+|[\*+^\/-]|[^0-9^,^\*+^\/-]*\(\S+\)|[^a-zA-Z0-9]+|\,|[a-zA-z]+)");
  return numbersAndStrings
      .allMatches(token)
      .map((RegExpMatch e) => getTokenForString(e.group(1), operators, symbols))
      .toList()
      .flatten()
      .cast<Token>();
}


List<Token> getTokenForString(String s, List<OperatorBuilder> operators, List<SymbolBuilder> symbols) {

  RegExp r = RegExp(r"[0-9]+");

  //Test if Function
  if (s.contains("(")) {
    return tokenizeFunctionBlock(s, operators, symbols);
  }

  //Test if number
  if (r.hasMatch(s)) {
    return [ValueToken(NumberValue( int.parse(s)))];
  }

  //Is operator or symbol
  return [getToken(s, operators, symbols)];
}

Token getToken(String s, List<OperatorBuilder> operators, List<SymbolBuilder> symbols) {
  //Optimize for default case
  switch (s) {
    case "+":
      return OperatorToken(OperatorSymbol.add, 1);
      break;
    case "-":
      return OperatorToken(OperatorSymbol.detract, 1);
    case "*":
      return OperatorToken(OperatorSymbol.multiply, 2);
    case "/":
      return OperatorToken(OperatorSymbol.divide, 2);
    case "^":
      return OperatorToken(OperatorSymbol.exponent, 4);
    case ",":
      return OperatorToken(OperatorSymbol.comma, -1);
  }

  if(operators!=null&&operators.any((OperatorBuilder operatorBuilder) => operatorBuilder.symbol==s)){
    OperatorBuilder match = operators.firstWhere((OperatorBuilder operatorBuilder) => operatorBuilder.symbol==s);
    return OperatorToken(OperatorSymbol.custom, match.priority, s);
  }

   if(symbols!=null&&symbols.any((SymbolBuilder builder) => builder.symbol==s)){
    SymbolBuilder match = symbols.firstWhere((SymbolBuilder builder) => builder.symbol==s);
    return ValueToken(match.build());
  }

  throw ParseException("Unexpected symbol ${s}");
}
