import 'package:dartcas/models/operators/OperatorBuilder.dart';
import 'package:dartcas/models/values/nonNumeric/Symbol.dart';

import 'TokenTypes.dart';
import 'Tokenizer.dart';

List<List<Token>> split(List<Token> list){
  
    List<List<Token>> splitted = [];
    List<Token> current = [];
    for(Token element in list){
      if(element is OperatorToken && element.action==OperatorSymbol.comma){
        if(current.isNotEmpty){
          splitted.add(current);
          current = [];
        }
      }else{
        current.add(element);
      }
    }
    if(current.isNotEmpty){
        splitted.add(current);
    }
    return splitted;
}

List<Token> tokenizeFunctionBlock(String functions,List<OperatorBuilder> operators, List<SymbolBuilder> symbols){

  List<Token> functionTokens = [];
  RegExp r = RegExp(r"\(");
  RegExp paramsRegExp = RegExp(r"\(\S+\)");
  List<String> functionBlocks = breakToFunctions(functions);

  //Test if function block contains more than one function 
  if(r.allMatches(functions).length>1&&functionBlocks.length>1){
    functionTokens.addAll(functionBlocks.map((String block) => tokenize(block, operators, symbols)).toList().flatten().cast<Token>());
  }else{
    RegExpMatch firstMatch = paramsRegExp.firstMatch(functions);
    String params = firstMatch.group(0);
    return [FunctionToken(functions.substring(0, firstMatch.start),
        split(tokenize(params.substring(1, params.length - 1), operators, symbols)))];
  }

  return functionTokens;
}

List<String> breakToFunctions(String s){
  List<String> functions = [];
  String current = "";
  int functionValue = 0;
  for(int i =0; i<s.length; i++){
    String curr = s[i];
    current +=curr;
    if(curr=="("){
      functionValue++;
    }
    else if (curr == ")"){
      functionValue--;
      if(functionValue ==0){
        functions.add(current);
        current="";
      }
    }
  }
  return functions;
}