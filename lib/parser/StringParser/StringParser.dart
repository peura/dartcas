import 'package:dartcas/models/functions/DefaultFunctions.dart';
import 'package:dartcas/models/functions/Function.dart';
import 'package:dartcas/models/operators/Operator.dart';
import 'package:dartcas/models/operators/OperatorBuilder.dart';
import 'package:dartcas/models/values/NumberValue.dart';
import 'package:dartcas/models/values/nonNumeric/Symbol.dart';
import 'package:dartcas/models/values/Value.dart';
import 'package:dartcas/parser/tokenizer/TokenTypes.dart';
import 'package:dartcas/parser/tokenizer/Tokenizer.dart';
import 'package:dartcas/models/operators/ValueOperator.dart';

import '../Parser.dart';
import 'Calculations.dart';
import 'settings/StringParserSettings.dart';

class StringParser extends Parser {

  final List<OperatorBuilder> operatorBuilders;

  List<FunctionOperator> _functions = [
    brackets,
    cosF,
    sqrtF,    
    sinF,
    max,
    min
  ];

  List<SymbolBuilder> _symbols=[
    pi.builder()
  ];


  @override
  Operator getParsed(String value) {
    
    List<Token> tokens = tokenize(cleanString(value), operatorBuilders, _symbols);

    if (tokens.length == 0) {
      return ValueOperator(value: NumberValue(0));
    }
    return parseTokens(tokens);
  }

  StringParserSettings _settings = StringParserSettings();

  StringParser({functions, this.operatorBuilders, settings, symbols}) {
    if (functions != null) {
      this._functions.addAll(functions);
    }
    if(settings!=null){
      _settings=settings;
    }
    if(symbols!=null){
      _symbols.addAll(symbols);
    }
  }

  String cleanString(String input){
    String cleaned = input;
    _settings.inputFormatters.forEach((element) { 
      cleaned = element.formatString(cleaned);
    });
    return cleaned;
  }

  ValueOperator calculateFunction(FunctionToken token, List<Value> params) {
    FunctionOperator match = _functions.firstWhere(
        (FunctionOperator function) => function.name == token.name, orElse: () {
          print(token.name);
      throw ParseException("Unknown function: ${token.name}");
    });
    return ValueOperator(value: match.functionRunner(params));
  }

  Operator parseTokens(List<Token> tokens) {
    int maxPrio = getMaxPriority(tokens);

    while (maxPrio > 1) {
      for (int i = 1; i < tokens.length; i++) {
        Token current = tokens[i];

        if (getPriority(current) == maxPrio) {
          ValueOperator selfAction;
          ValueToken otherValue;

          selfAction =
              ValueOperator(value: tokenToValueToken(tokens[i - 1]).value);

          otherValue = tokenToValueToken(tokens[i + 1]);

          Operator action = getOperatorForToken(selfAction, current, otherValue, operatorBuilders);
          ValueToken result = ValueToken(action.getValue());
          tokens.removeRange(i - 1, i + 2);
          tokens.insert(i - 1, result);
          i--;
        }
      }
      maxPrio = getMaxPriority(tokens);
    }

    if (tokens.length == 1) {
      Token result = tokens[0];
      if (result is ValueToken || result is FunctionToken) {
        return ValueOperator(value: tokenToValueToken(result).value);
      } else {
        throw ParseException("Invalid syntax! Unexpected token at 0 ${tokens}");
      }
    }

    ValueToken start = tokenToValueToken(tokens[0]);
    ValueOperator startAction = ValueOperator(value: start.value);
    Operator currentAction = getOperatorForToken(
        startAction, tokens[1], tokenToValueToken(tokens[2]), operatorBuilders);

    for (int i = 3; i < tokens.length; i += 2) {
      currentAction = getOperatorForToken(
          currentAction, tokens[i], tokenToValueToken(tokens[i + 1]), operatorBuilders);
    }
    return currentAction;
  }

  ValueToken tokenToValueToken(Token token) {
    if (token is ValueToken) {
      return token;
    } else if (token is FunctionToken) {
      return ValueToken(
          calculateFunction(token, token.params.map((List<Token> tokens) => parseTokens(tokens).getValue()).toList())
              .getValue());
    }
    throw ParseException(
        "Unexpected token type ${token.runtimeType.toString()}");
  }

}

class ParseException implements Exception {
  ParseException(this.cause);
  String cause;
}

class ExponentParameterException implements Exception {
  ExponentParameterException(this.cause);
  String cause;
}