import 'dart:math';
import 'package:dartcas/models/operators/Division.dart';
import 'package:dartcas/models/operators/Exponent.dart';
import 'package:dartcas/models/operators/Multiplication.dart';
import 'package:dartcas/models/operators/Operator.dart';
import 'package:dartcas/models/operators/OperatorBuilder.dart';
import 'package:dartcas/models/operators/Sum.dart';
import 'package:dartcas/models/operators/ValueOperator.dart';
import 'package:dartcas/parser/StringParser/StringParser.dart';
import 'package:dartcas/parser/tokenizer/TokenTypes.dart';


int getMaxPriority(List<Token> tokens) {
  return [for (Token token in tokens) getPriority(token)].reduce(max);
}

int getPriority(Token token) {
  if (token is ValueToken) {
    return 0;
  }

  if (token is FunctionToken) {
    return 0;
  }

  if (token is OperatorToken) {
    return token.priority;
  }

  return 0;
}

Operator getOperatorForToken(
    Operator current, OperatorToken action, ValueToken number,
    [List<OperatorBuilder> operators]) {
  ValueOperator value = ValueOperator(value: number.value);

  switch (action.action) {
    case OperatorSymbol.add:
      return Sum(self: value, other: current);
    case OperatorSymbol.detract:
      return Detraction(self: current, other: value);
    case OperatorSymbol.divide:
      return Division(self: current, other: value);
    case OperatorSymbol.multiply:
      return Multiplication(self: value, other: current);
    case OperatorSymbol.exponent:
      return Exponent(self: current, other: value);
    case OperatorSymbol.custom:
      OperatorBuilder matchingOperatorBuilder = operators.firstWhere(
          (OperatorBuilder o) => o.symbol == action.token, orElse: () {
        throw ParseException("Unexpected value: ${action.token}");
      });
      return matchingOperatorBuilder.buildOperator(current, value);
      break;
    case OperatorSymbol.none:
      throw ParseException("Reached none block");
    default:  
      throw ParseException("Unexpected value: ${action.token}");
  }
}
