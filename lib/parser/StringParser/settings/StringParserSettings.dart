
import 'InputFormatter.dart';

class StringParserSettings{

  List<InputFormatter> inputFormatters = [
    WhiteSpaceRemoverInputFormatter()
  ];
  CalcMode calcMode = CalcMode.accurate;
  InputType inputType = InputType.normal;

}

enum CalcMode{
  accurate,
  approximate
}

enum InputType{
  auto,
  latex,
  normal
}