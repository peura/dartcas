abstract class InputFormatter{
  String formatString (String input);
}

class WhiteSpaceRemoverInputFormatter extends InputFormatter{
  @override
  String formatString(String input) {
    return input.replaceAll(" ", "");
  }
}