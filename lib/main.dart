import 'dart:io';

import 'package:args/args.dart';
import 'package:dartcas/parser/StringParser/StringParser.dart';
import 'parser/StringParser/StringParser.dart';

const lineNumber = 'line-number';

ArgResults argResults;

void main(List<String> arguments) {
  exitCode = 0; 
  final parser = ArgParser()
    ..addFlag(lineNumber, negatable: false, abbr: 'n');

  argResults = parser.parse(arguments);
  final paths = argResults.rest;


  StringParser stringParser = StringParser();
  print(stringParser.getParsed(paths[0]).getValue());
}